# oh-my-zsh

## On ubuntu

### Install zsh and oh-my-zsh

First,install zsh because oh-my-zsh is based on this shell.
```
$sudo apt-get install zsh
```
Then install oh-my-zsh with curl or wget(if you do not have these tools,just install them first.)
```
#via curl 
$sh -c "$(curl -fsSL http://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

#via wget
$sh -c "$(wget http://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O-)"
```
Now,you finish installing.

### Modify configuration of oh-my-zsh

Set zsh as default shell.
```
$chsh -s /bin/zsh
```

## On CentOS

# Powerlevel10k theme

Because Powerlevel10k is not a built-in theme,we need to download it first.

```
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
```

Then change the theme in .zshrc setting file.
```
ZSH_THEME="powerlevel10k/powerlevel10k"
```
To see complete icons,adding this ine in .zshrc setting file.
```
POWERLEVEL10K_MODE='nerdfont-complete'
```
Configure powerlevel10k in terminal
```
p10k configure
```
More Powerlevel10k setting
```
vim ~/.p10k.zsh
```
